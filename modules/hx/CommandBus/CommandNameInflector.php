<?php

/**
 * Description of CommandNameInflector
 *
 * @author erickjose
 */


class CommandNameInflector {

    public function getHandler(CommandInterface $command) {
    	//Set injector for handler class.
    	$injector = new Auryn\Injector;
        return $injector->make(str_replace('Command', 'Handler', get_class($command)));
    }

}
