<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommandBus
 *
 * @author erickjose
 */
class CommandBus implements CommandBusInterface {
    
    
    /**
     * @var CommandNameInflector
     */
    private $inflector;


    public function __construct(CommandNameInflector $inflector)
    {
        $this->inflector = $inflector;
    }
    
    public function execute(CommandInterface $command)
    {
        $handler = $this->getHandler($command);
        return $handler->handle($command);
    }
    
    private function getHandler($command)
    {
        return $this->inflector->getHandler($command);
    }
    
    public function __call($name, $arguments) {
        
        $base_path_class = Kohana::$config->load('hx')->base_path;
        $dir_name = Kohana::$config->load('hx')->dir_name;
        
        $name_parts = explode("_", $name);
        
        $command = null;
                
        if( count($name_parts) == 1 ){
            //Default command
            $path = $dir_name.DIRECTORY_SEPARATOR."Default".DIRECTORY_SEPARATOR."Commands";
            $file = $name_parts[0]."Command";
            $classname = $dir_name."_Default_Commands_".$file;
        }elseif( count($name_parts) == 2 ){
            $path = $dir_name.DIRECTORY_SEPARATOR.$name_parts[0].DIRECTORY_SEPARATOR."Commands";
            $file = $name_parts[1]."Command";
            $classname = $dir_name."_".$name_parts[0]."_Commands_".$file;            
        }else{
            throw new Kohana_Exception("Invalid class name ");
        }

        if (Kohana::find_file($base_path_class, $path.DIRECTORY_SEPARATOR.$file)) {
            $command = new ReflectionClass($classname);
            return $command->newInstance($arguments);
        } else {
            throw new Kohana_Exception("File for class '" . $classname . "' not found in Defaults! ");
        }
        
        
    }


}
