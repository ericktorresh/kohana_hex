<?php
/**
 *
 * @author erickjose
 */
interface CommandBusInterface {
    public function execute(CommandInterface $command);
}
