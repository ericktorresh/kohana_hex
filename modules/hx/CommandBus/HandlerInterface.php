<?php
/**
 *
 * @author erickjose
 */
interface HandlerInterface {
    public function handle(CommandInterface $command);
}
