<?php

interface RepositoryInterface {
    public function save(ORM $model);
}