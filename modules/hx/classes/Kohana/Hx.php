<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Library for implementation Hexagonal Archecteture. 
 * @package    Kohana/Hx
 * @author     Erick Torres
 * @copyright  (c) 2015
 */

class Kohana_Hx {
    protected $_config;

    /**
     * Loads configuration options.
     *
     * @param   array  $config  Config Options
     * @return  void
     */
    public function __construct($config = array()) {
        // Save the config in the object
        $this->_config = $config;
    }

    public  static function get_command_bus() {
        return new CommandBus( new CommandNameInflector );
    }
	

} // End Hx
