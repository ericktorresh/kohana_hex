<?php

defined('SYSPATH') or die('No direct script access.');

//Include vendor composer
include_once Kohana::find_file('vendor', 'autoload');

// Autoloader
class HX_Autoloader {

    public static function autoload($class) {
        if (is_file(Kohana::find_file('CommandBus', $class)))
            include_once Kohana::find_file('CommandBus', $class);

        if (is_file(Kohana::find_file('Validation', $class)))
            include_once Kohana::find_file('Validation', $class);
    }

}

// Register the autoloader
spl_autoload_register(array('HX_Autoloader', 'autoload'));


