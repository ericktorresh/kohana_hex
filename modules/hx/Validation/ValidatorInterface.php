<?php

/**
 *
 * @author erickjose
 */
interface ValidatorInterface {

    public function validate(CommandInterface $command);
    
}
