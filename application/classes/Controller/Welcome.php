<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

    private $bus;
    
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->bus =  Hx::get_command_bus() ;
    }

    public function action_index() {

        //Data for testing
    	$command = $this->bus->User_CreateUser(
    		array(
    			"firstname"=>"Test",
    			"lastname"=>"User",
    			"email"=>"t.com" //Invalid email
    		)
    	);
    	//Execute command 
        $errors = array();
        $user = array();
        
        try{
            $user = $this->bus->execute($command);
        } catch (ORM_Validation_Exception $e) {
            $errors = $e->errors('');
        }
    	
        var_dump($errors);
    	var_dump($user);

        $this->response->body('Testing!!!');
    }

}

// End Welcome
