<?php

/**
 * Description of CreateUserValidator
 *
 * @author erickjose
 */
class HX_User_Validators_CreateUserValidator implements ValidatorInterface {

    private $validator;
    
    protected $rules = array(
        'firstname' => array(
            array('not_empty'),
            array('min_length', array(':value', 4)),
            array('max_length', array(':value', 32)),
            array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
        ),
        'lastname' => array(
            array('not_empty'),
            array('min_length', array(':value', 4)),
            array('max_length', array(':value', 32)),
            array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
        ),
        'email' => array(
            array('not_empty'),
            array('min_length', array(':value', 4)),
            array('max_length', array(':value', 127)),
            array('email'),
        ),
    );

    private function set_rules(){
        if (count($this->rules)){
            foreach ($this->rules as $field=>$rules){
                $this->validator->rules($field, $rules);
            }
        }
    }

    public function validate(\CommandInterface $command) {
        $this->validator = new Validation($command->data);
        $this->set_rules();
        
        if (!$this->validator->check())
        {
            throw new ORM_Validation_Exception('', $this->validator);
        }
    }

}
