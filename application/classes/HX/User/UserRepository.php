<?php

class HX_User_UserRepository implements RepositoryInterface {
    /**
     * @var User
     */
    private $user;

    public function __construct(HX_User_User $user)
    {
        $this->user = $user;
    }

    public function save(ORM $user)
    {
        return $user->save();
    }
} 