<?php  

class HX_User_Handlers_CreateUserHandler implements HandlerInterface {

    private $validator;
    private $repository;
    private $dispatcher;

    public function __construct(HX_User_UserRepository $repository, HX_User_Validators_CreateUserValidator $validator)
    {
        $this->validator = $validator;
        $this->repository = $repository;
        //$this->dispatcher = $dispatcher;
    }
    public function handle(CommandInterface $command)
    {
        $this->validate($command);
        return $this->save($command);
    }
    protected function validate($command)
    {
        $this->validator->validate($command);
    }
    protected function save($command)
    {
        $user = new HX_User_User;
        $user->firstname = $command->firstname;
        $user->lastname = $command->lastname;
        $user->email = $command->email;
        return $this->repository->save($user);
        //$this->dispatcher->dispatch( $ticket->flushEvents() );
    }
} 