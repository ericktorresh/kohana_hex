<?php

defined('SYSPATH') or die('No direct script access.');

return array
    (
    'firstname' => array
        (
        'not_empty' => 'your message',
        'max_length' => 'your message',
        'alpha_dash' => 'your message',
        'default' => 'default message'
    ),
);
